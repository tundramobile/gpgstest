#pragma once
#include <cocos2d.h>
#include "MainScene.hpp"

namespace gt
{
    
    class Application: private cocos2d::Application
    {
    public:
        static Application* instance();
        
        virtual bool applicationDidFinishLaunching() override final;
        virtual void applicationDidEnterBackground() override final;
        virtual void applicationWillEnterForeground() override final;
		
	private:
		std::shared_ptr<MainScene> mMainScene;
        
    };
    
}
