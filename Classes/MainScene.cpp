#include <gpg/gpg.h>
#include "MainScene.hpp"

class gt::MainLayer: public cocos2d::LayerColor
{
public:
	virtual bool init();
	CREATE_FUNC(MainLayer)
	
protected:
	void updateLabels();
	
private:
	gpg::PlatformConfiguration mPlatformConfiguration;
	std::shared_ptr<gpg::GameServices> mGameServices;
	
	cocos2d::Label *mStatus;
	cocos2d::MenuItemImage *mButton;
	
};

using namespace gt;

bool MainLayer::init()
{
	cocos2d::Size visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
	if (!cocos2d::LayerColor::initWithColor(cocos2d::Color4B::WHITE, visibleSize.width, visibleSize.height))
	{
		return false;
	}
	
	mPlatformConfiguration.SetClientID("571410966257-dcuoujqj3pv7act7sbs49ln3t9sbj7f8.apps.googleusercontent.com");
	
	mButton = nullptr;
	mStatus = nullptr;
	
	mGameServices = std::shared_ptr<gpg::GameServices>(gpg::GameServices::Builder()
		.SetDefaultOnLog(gpg::LogLevel::VERBOSE)
		.SetOnAuthActionStarted([this](gpg::AuthOperation op)
		{
			if (mButton)
				mButton->setEnabled(false);
			if (mStatus)
				mStatus->setString("Signing In...");
		})
		.SetOnAuthActionFinished([this](gpg::AuthOperation op, gpg::AuthStatus status)
		{
			if (mButton)
				mButton->setEnabled(true);
			updateLabels();
		})
		.Create(mPlatformConfiguration));
	
	mStatus = cocos2d::Label::createWithSystemFont("Signed Out", "AvenirNext-Regular", 48);
	mStatus->setAnchorPoint(cocos2d::Vec2::ANCHOR_MIDDLE_LEFT);
	mStatus->setColor(cocos2d::Color3B::BLACK);
	mStatus->setPosition(cocos2d::Vec2(visibleSize.width * 0.5 + 64, visibleSize.height * 0.5));
	addChild(mStatus);
	
	mButton = cocos2d::MenuItemImage::create("gpp_sign_in_normal.png",
												 "gpp_sign_in_pressed.png",
												 "gpp_sign_in_disabled.png",
												 [this](cocos2d::Ref *) -> void
	{
		if (mGameServices->IsAuthorized())
		{
			mGameServices->SignOut();
		}
		else
		{
			mGameServices->StartAuthorizationUI();
		}
	});
	mButton->setPosition(cocos2d::Vec2(visibleSize.width * 0.5, visibleSize.height * 0.5));
	auto menu = cocos2d::Menu::create(mButton, nullptr);
	menu->setPosition(cocos2d::Vec2::ZERO);
	addChild(menu);
	
	return true;
}

void MainLayer::updateLabels()
{
	if (mStatus)
	{
		mStatus->setString(mGameServices->IsAuthorized() ? "Sign Out" : "Sign In");
	}
}

bool MainScene::init()
{
	if (!cocos2d::Scene::init())
	{
		return false;
	}
    
	mLayer = std::shared_ptr<MainLayer>(MainLayer::create());
	mLayer->retain();
	addChild(mLayer.get());
    
	return true;
}
