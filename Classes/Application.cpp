#include "Application.hpp"

using namespace gt;

Application* Application::instance()
{
	return reinterpret_cast<Application *>(getInstance());
}

bool Application::applicationDidFinishLaunching()
{
	auto director = cocos2d::Director::getInstance();
	auto glView = director->getOpenGLView();

	if (!glView)
	{
		glView = cocos2d::GLView::create("Google Play Game Services Test");
		director->setOpenGLView(glView);
	}
	director->setAnimationInterval(1.0 / 30.0);

	mMainScene = std::shared_ptr<MainScene>(MainScene::create());
	mMainScene->retain();
	director->runWithScene(mMainScene.get());

	return true;
}

void Application::applicationDidEnterBackground()
{
	cocos2d::Director::getInstance()->stopAnimation();
}

void Application::applicationWillEnterForeground()
{
	cocos2d::Director::getInstance()->startAnimation();
}
