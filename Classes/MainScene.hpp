#pragma once
#include <memory>
#include <cocos2d.h>

namespace gt
{
    
	class MainLayer;
    
	class MainScene: public cocos2d::Scene
	{
	public:
		virtual bool init();
		CREATE_FUNC(MainScene)
		
	protected:
		std::shared_ptr<MainLayer> mLayer;
		
	};
    
}
