Steps:

1. Create game + iOS app in Google Play dev. console, get oauth client ID.
2. Handle URL on iOS:

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [GPPURLHandler handleURL:url sourceApplication:sourceApplication annotation:annotation];
}

+ URL Scheme = bundleID.